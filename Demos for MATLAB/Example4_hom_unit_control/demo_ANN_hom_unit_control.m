%%% hANN for homogeneous unit control and simulations  

A=[0 3 0 0 0;
   -3 0 0 0 0;
   0 0 1 -1 1;
   0 1 0 2 1;
   1 0 1 -1 3];  %  system matrix

B=[0 0;
   0 0;
   0 0;
   1 0;
   0 1]; % control matrix

C=[0 0 1 0 0;
   0 0 0 1 0;
   0 0 0 0 1]; % sliding surface  Cx=0

n=5;m=2; p=3;
rho=1; %convergence rate tuning parameter (learger rho faster the convergence) 
gamma_max=0.5; % 0.4 - magnitude of perturbation + 0.1 gap for approximation error 

[K0 K Gd P]=hsmc_design(A,B,C,rho,gamma_max); %design the parameteres of hom. unit SMC 

%K0 - homogenization gain
%K  - control gain
%Gd - generator of dilation
%P  - shape matrix of the weighted Euclidean norm



%%%% definition of an explicit hom norm %%%


rb=1;                                                                      
[Q L J]=approx_hnorm_weight(Gd,P,rb,30);            %param. of explicit hom. norm

hn_fun=@(x)hnorm_weight(x,L,rb,Q,J);               %explicit hom. norm


%%%% Approximation of hom. proj by hANN %%%

[xi Theta W]=approx_hproj_sigmoid(Gd,P,hn_fun,10);


%%%%%%  check an approximation error
%x=[1;2;3];
%hann(x,Theta,xi,W,Gd,0,hn_fun)
%hproj(x,Gd,P)
%return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

t=0; Tmax=4; 
h=0.001; % sampling period

x=[1;0;2;0;1];
tl=[t];xl=[x];ul=[];



noise=0.00;    % magnitude of measurement noises 

disp('Run numerical simulation...');

[Ah, Bh]=ZOH(h,A,B);
tic
while t<Tmax
    xm=x+2*noise*(rand(5,1)-0.5);                  %(possibly) noised state

    %u=(K0+K*C)*xm;                       % linear control (for comparison)
      
    u=K0*xm+K*hann(C*xm,Theta,xi,W,Gd,0,hn_fun);       % explicit discretization of HSMC
    
    %u=si_hsmc(h,xm,A,B,C,K0,K,Gd,P,alpha); %semi-implicit discret. of HSMC
    
    gamma_t=(gamma_max-0.1)*[sin(2*t);cos(10*t)];          % matched perturbation 

    x=Ah*x+Bh*(u+gamma_t);                        %simulation of the system

    t=t+h;
    tl=[tl t];
    xl=[xl x];
    ul=[ul u];
end;
toc
ul=[ul u];

disp('Done!');

%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot simulation results
%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
axes1 = subplot(1,2,1);
hold(axes1,'on');
plot1 = plot(tl,xl,'LineWidth',2,'Parent',axes1);
set(plot1(1),'DisplayName','$x_1$');
set(plot1(2),'DisplayName','$x_2$');
set(plot1(3),'DisplayName','$x_3$');
set(plot1(4),'DisplayName','$x_4$');
set(plot1(5),'DisplayName','$x_5$');
ylabel('$x$','Interpreter','latex');
xlabel('$t$','Interpreter','latex');
title({'n=5'});
xlim(axes1,[0 Tmax]);
ylim(axes1,[-3 3]);
box(axes1,'on');
hold(axes1,'off');
set(axes1,'FontSize',30,'XGrid','on','YGrid','on');
legend1 = legend(axes1,'show');
set(legend1,'Interpreter','latex');


axes2 = subplot(1,2,2);
hold(axes2,'on');
plot2 = plot(tl,ul,'LineWidth',2);
set(plot2(1),'DisplayName','$u_1$');
set(plot2(2),'DisplayName','$u_2$');
ylabel('$u$','Interpreter','latex');
xlabel('$t$','Interpreter','latex');
title({'HSMC,m=2'});
xlim(axes2,[0 Tmax]);
ylim(axes2,[-12 12]);
box(axes2,'on');
hold(axes2,'off');
set(axes2,'FontSize',30,'XGrid','on','YGrid','on');
legend2 = legend(axes2,'show');
set(legend2, 'Interpreter','latex');






