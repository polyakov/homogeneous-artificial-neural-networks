function y=calc_xi(s,bi)
[n m]=size(s);
zx=n/2;zy=m/2;
y=0;
for i=1:n
    for j=1:m
    y=y+s(i,j)*bi(i-zx,zy-j);
    end;
end;