function [A B C]=train_hann_rec(img_list,N_)
% Least square traing of homogeneous ANN 
% Input parameters: the image list and the number of neurons N_

n=8; %number of inputs 
k=8; %number of neurons in the output layer
%N_ is a number of neurons in the hidden layer
A=randn(N_,n);   
B=randn(N_,1);

Gd=diag([2 3 3 4 4 4 5 5]);%generator of the dilation
hn_fun=@(x)hnorm_weight(x,diag(Gd));%homogeneous norm
sigma=@(r) 1./(1+exp(r));%sigmoid activation function

%traing rule J:=(W*C-Q)^2 -> min_C

E=eye(k);
W=[]; Q=[]; 

for i=1:8
img=cell2mat(img_list(i));
phi=abs((double(img(:,:,1))+double(img(:,:,2))+double(img(:,:,3)))/3-255);%make image gray scale 
x=calc_x(phi);
W=[W sigma(A*expm(-log(hn_fun(x))*Gd)*x+B)];
Q=[Q E(:,i)];

img_zoom_05=nnbr(img,0.5);%image zoom 50%
phi_zoom_05=abs((double(img_zoom_05(:,:,1))+double(img_zoom_05(:,:,2))+double(img_zoom_05(:,:,3)))/3-255);%(make image gray scale )
x_zoom_05=calc_x(phi_zoom_05);
W=[W sigma(A*expm(-log(hn_fun(x_zoom_05))*Gd)*x_zoom_05+B)];
Q=[Q E(:,i)];

img_zoom_035=nnbr(img,0.35);%image zoom 35%
phi_zoom_035=abs((double(img_zoom_035(:,:,1))+double(img_zoom_035(:,:,2))+double(img_zoom_035(:,:,3)))/3-255);%(make image gray scale )
x_zoom_035=calc_x(phi_zoom_035);
W=[W sigma(A*expm(-log(hn_fun(x_zoom_035))*Gd)*x_zoom_035+B)];
Q=[Q E(:,i)];

img_zoom_075=nnbr(img,0.75);%image zoom 75%
phi_zoom_075=abs((double(img_zoom_075(:,:,1))+double(img_zoom_075(:,:,2))+double(img_zoom_075(:,:,3)))/3-255);%(make image gray scale )
x_zoom_075=calc_x(phi_zoom_075);
W=[W sigma(A*expm(-log(hn_fun(x_zoom_075))*Gd)*x_zoom_075+B)];
Q=[Q E(:,i)];
end;

C=Q/W;
return

