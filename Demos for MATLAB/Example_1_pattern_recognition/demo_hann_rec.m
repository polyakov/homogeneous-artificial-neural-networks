%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Example of homogenenous Artificial Neural Network (hANN)
%%   
%%  for zoom-independent pattern recognition
%%                

%%      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
%%%%%%%%%%%%%%%%%%%%%
%% Download images
%%%%%%%%%%%%%%%%%%%%%
img1=imread('training_set/sym_1.png');%image(img1)
img2=imread('training_set/sym_2.png');%image(img2)
img3=imread('training_set/sym_3.png');%image(img3)
img4=imread('training_set/sym_4.png');%image(img4)
img5=imread('training_set/sym_5.png');%image(img5)
img6=imread('training_set/sym_6.png');%image(img6)
img7=imread('training_set/sym_7.png');%image(img7)
img8=imread('training_set/sym_8.png');%image(img8)

img_list={img1, img2, img3, img4, img5, img6, img7, img8};


%image(uint8(nnbr(img8,0.3))); show a image zoomed at 30% (if needed) 

%%%%%%%%%%
%% Homogeneity parameters of ANN
%%%%%%%%%%
Gd=diag([2 3 3 4 4 4 5 5]);
hn_fun=@(x)hnorm_weight(x,diag(Gd)); %definition of homogeneous norm 
%%%%

%% Traing/loading hANN
%%%
ok=0;
%while ok==0
%[A B C]=train_hann_rec(img_list,8);%traing of the network 
%save("ABC_HANN.mat","A","B","C"); save the trained network
load('ABC_HANN.mat'); %loading of the already trained network

%%%%%%%%%%%%%%%%%%%%%%%%
%% Test of homogeneous ANN
%%%%%%%%%%%%%%%%%%%%%%%%
ok=1;
for i=1:8
 img=cell2mat(img_list(i));
 for j=0.5:0.1:2 %tested for the well-trained network with j=0.3:0.1:10
    img_zoom=nnbr(img,j);%image zoom at j*100%
    
    phi_zoom=abs((double(img_zoom(:,:,1))+double(img_zoom(:,:,2))+double(img_zoom(:,:,3)))/3-255);%make image gray scale
    x_zoom=calc_x(phi_zoom);%calculation of x

     hv=hann(x_zoom,A,B,C,Gd,0,hn_fun); %recognition by Homegeneous ANN

     [v ni]=max(hv); %ni - the number of the detected symbol 

     if i~=ni 
         ok=0;
         disp('Recognition error: zoom test');
         i
         j
        end;   
    end;
%generation of the noise
noise=20*randn(size(img,1),size(img,2));
noise3d=zeros(size(img));
noise3d(:,:,1)=noise;noise3d(:,:,2)=noise;noise3d(:,:,3)=noise;
noise3d=uint8(noise3d);

img_noisy=max(img-noise3d,noise3d); %addinig a noise to the nominal image
phi_noisy=abs((double(img_noisy(:,:,1))+double(img_noisy(:,:,2))+double(img_noisy(:,:,3)))/3-255);%make image gray scale
x_noisy=calc_x(phi_noisy);%calculation of x
hv=hann(x_noisy,A,B,C,Gd,0,hn_fun);%recognition by Homegeneous ANN
[v ni]=max(hv);
if i~=ni ok=0; disp('Recognition error: noize test'); 
         i
        end;
end;
if ok==1 disp('No recognition errors'); end;



