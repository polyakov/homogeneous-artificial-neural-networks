function imzoom = nnbr(image,zoom);
[r c d] = size(image);      % dimensions of image data
%% zoom
rn = floor(zoom*r);
cn = floor(zoom*c);
s = zoom;
im_zoom = zeros(rn,cn,d);
%% nearest neighbour
for i = 1:rn;
    x = i/s;
    near_i = cast(round(x),'uint8');
if near_i == 0
        near_i = 1;
    end
    for j = 1:cn;
        y = j/s;
near_j = cast(round(y),'uint8');
        if near_j == 0
            near_j = 1;
        end
        im_zoom(i,j,:) = image(near_i,near_j,:);
    end
end
imzoom = im_zoom;