function x=calc_x(s)
%%%%%%%%%%%%%%%%%%
% Basis functions
%%%%%%%%%%%%%%%%%%
koef=1;
b1=@(x,y) 1/koef; b2=@(x,y) x/koef; b3=@(x,y) y/koef; b4=@(x,y) 0.5*x*y/koef;
b5=@(x,y) 0.5*x*x/koef; b6=@(x,y) 0.5*y*y/koef; b7=@(x,y) x*x*y/koef/6; b8=@(x,y) 0.5*x*y*y/koef/6;

x=[calc_xi(s,b1);calc_xi(s,b2);calc_xi(s,b3);calc_xi(s,b4);calc_xi(s,b5);calc_xi(s,b6);calc_xi(s,b7);calc_xi(s,b8)];