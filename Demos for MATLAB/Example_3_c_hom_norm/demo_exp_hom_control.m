%%% simulation of planar system with explicit homogeneous control 

%system model 
n=2; m=1;

A=[0 1;0 0]; B=[0;1]; eps=0.005;

%dilation 
mu=-0.25; 
Gd=[1-mu 0; 0 1]; iGd=inv(Gd);

%control parameters 
rho=1;
P=[0.1157   0.0194;
   0.0194   0.1186]; K=[-0.8770   -3.0839];

% explicit homogeneous norm \bar r
L=[1.0000; 1.2500];
J=[0 1;1 0];
Q= [0.0720 0.0176
    0.0176  0.1225];
rb=5/4;

% homogeneous norm by ANN
W=[0.3811   -0.4087    0.2896    0.0763   -0.1991   -0.0017   -0.3500   ...
    0.3018    0.1019    0.0572   -0.3244   -0.4511    0.2717];
q=[-0.1577   -0.1892   -0.1131    0.1575    0.2247    0.3261    0.2162   ...
    -0.3548   -0.2596   -0.4699   -0.4272    0.4025   -0.4054;
   -0.3018    0.2132   -0.3004    0.1802   -0.4246    0.3039   -0.1420   ...
       0.1574   -0.2215    0.3211    0.3809    0.1276    0.1615];
U=[55.4541  -35.5287  -59.6165   18.7982   -0.9732   13.0627  -10.9223   ...
    1.0197   24.9912   -3.7383    8.3027   -0.0992    2.4333];

h_fun=@(x)hnorm_sigmoid(x,Gd,W,q,U,@(x)hnorm_weight(x,L,rb,Q,J));% ANN=U*sigma(W*x+q)

%numerical simulation of control system

t=0;x=[1;0];
tl=[];xl=[];ul=[];

Tmax=15; h=0.001;
while t<=Tmax
 hn=h_fun(x); %computation of homogenenous norm
 u=K*expm(-(Gd-(mu+1)*eye(n))*log(hn))*x; %computation of the control
 t=t+h;
 x=x+h*(A*x+B*(u+cos(10*t*hn)^2*hn^(1+mu))); %Euler method
 tl=[tl t];
 xl=[xl,x];
 ul=[ul u];
end;    
figure
plot(tl,xl)
figure
plot(tl,ul)




