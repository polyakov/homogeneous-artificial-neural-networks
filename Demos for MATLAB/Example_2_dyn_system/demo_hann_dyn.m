%%%%%%%%%%%%
%%Example of homogeneous extrapolation of ANN approximating  a model of rotation dynamics
%%%%%%%%%%%%
n=6; %number of inputs 

%[A,B,C]=train_ann_dyn(500); %training of ANN
% save("ABC_HANN.mat","A","B","C"); %save of the parameters of ANN
load('ABC_ANN.mat') %loading the papertes of a trained ANN

%%%%%%%%%%%%
%%Identification of practical dilation (takes 1-2 min)
%%%%%%%%%%%%
disp(' ');
disp('-Identification of practical dilation (takes 1-2 min)');

%Generatation of points on the set Omega_delta (close to the unit sphere)
xl=[];Nx=4000;
while size(xl,2)<Nx
 x=(rand(n,1)-0.5)*1.05;
 nx=norm(x);
 if (nx>0.98) && (nx<=1.02) xl=[xl x];end;
end;

delta=0.01; %the radius of the ball B_delta

E=eye(n);sm=zeros(3,1); 
Wl=[];Ql=[];
for i=1:Nx
         lambda=zeros(n,1);
         xst=xl(:,i);
         x=xst+delta*ones(n,1);
         w_line=zeros(3,n); q_line=zeros(3,1);
        for k=1:n^2
          y=x+diag(lambda)*delta*ones(n,1);
          gy=ann(y,A,B,C);
          w_line_k=[];
            for p=1:n                
                Ep=E(:,p).*E(:,p)';
                 gx=ann(y+Ep*(x-y),A,B,C);
                 gxst=ann(y+Ep*(xst-y),A,B,C);
                  w_line_k=[w_line_k, (-gx*x(p,1)+gxst*xst(p,1))/(x(p,1)-xst(p,1))+gy];         
                end;
          q_line=q_line+gy;
          w_line=w_line+w_line_k;
          p=1;
          while lambda(p,1)==1
          lambda(p,1)=0;
          p=p+1;
          end;
          lambda(p,1)=1;
         end;
         Wl=[Wl; w_line]; Ql=[Ql;q_line];
end
disp('The identified generator of the practical dilation (for degree 1) is')
G_eps=diag(-Wl\Ql) %the approximated generator of dilation for nu=1


Gd=diag([0.5 0.5 0.5 1 1 1]);% the exact generator of dilation for the degree 1                                  
hn_fun=@(x)hnorm(x,Gd,eye(6)); % the canonical homogeneous norm induced by the Euclideant norm |x|=sqrt(x'*x)


%%%%%%%%%%%%%%%%%%%%%
%Identification of practical homogenety degree (<30 sec)
%%%%%%%%%%%%%%%%%%%%%
disp(' ');
disp('--Identification of practical homogenety degree');

nu_eps=0;K=20000;delta=0.01;
is_hom=1;
for i=1:K
 %data generation
xl=[];
while size(xl,2)<1
 x=(rand(n,1)-0.5)*1.05;
 nx=norm(x);
 if (nx>=.95) && (nx<=1.05) xl=[xl x];end;
end;

s=0;NM=0;
    hnx=hn_fun(xl(:,1));
    gx=ann(xl(:,1),A,B,C);
    gpi=ann(expm(-Gd*log(hnx))*xl(:,1),A,B,C);
    if (abs(gx(1,1))>delta) && (abs(gpi(1,1))>delta)
      if   gx(1,1)/gpi(1,1)<0 s_hom=0;        %homogeneity test    
                         else s=s+log(gx(1,1)/gpi(1,1))/log(hnx);
                              NM=NM+1;
      end;
    end;
    if (abs(gx(2,1))>delta) && (abs(gpi(2,1))>delta)
        if   gx(2,1)/gpi(2,1)<0 is_hom=0;       %homogeneity test     
        else    s=s+log(gx(2,1)/gpi(2,1))/log(hnx);
                NM=NM+1;
        end;
    end;
      if (abs(gx(3,1))>delta) && (abs(gpi(3,1))>delta)
         if   gx(3,1)/gpi(3,1)<0   is_hom=0;        %homogeneity test  
         else    s=s+log(gx(3,1)/gpi(3,1))/log(hnx);
                 NM=NM+1;
         end;
    end;   
end;
if is_hom==0 disp('Error: the ANN is non-homogeneous or approximation precision of ANN is not enough to detect a homogeneity');
else disp('The detect practical homogeneity degree is') 
    nu_eps=s/NM
end;




%%%%%%%%%%%
%%Comparion of ANN and hANNs
%%%%%%%%%%%
disp(' ');
disp('---Comparion of ANN and hANNs (takes 1-2 min)'); 

%Estimation of approximation error in the set r1<|x|<=r2

r1=0;r2=0.25;
while r2<2.00001
s=0;s2=0;Ntests=20000;err_inf_1=0;err_inf_2=0;err_inf_3=0;err_inf_4=0;
for k=1:Ntests
%data generation
xl=[];gxl=[];
while size(xl,2)<1
 x=(rand(n,1)-0.5)*r2; 
 nx=norm(x);
 if (nx>r1) && (nx<=r2) xl=[xl x]; gxl=[gxl rhs(x)];end;
end;


eps1=norm(ann(xl(:,1),A,B,C)-gxl(:,1));%error of the conventional ANN

eps2=norm(hann(xl(:,1),A,B,C,Gd,1,hn_fun)-gxl(:,1)); %error of hANN with known Gd=[...] and known nu=2

eps3=norm(hann(xl(:,1),A,B,C,Gd,nu_eps,hn_fun)-gxl(:,1)); %error of hANN with known Gd=[...] and unknown nu

eps4=norm(hann(xl(:,1),A,B,C,G_eps,1,hn_fun)-gxl(:,1)); %error of hANN with unknown Gd

err_inf_4=max(eps4,err_inf_4);
err_inf_3=max(eps3,err_inf_3);
err_inf_2=max(eps2,err_inf_2);
err_inf_1=max(eps1,err_inf_1);
end;
%estimate of approximation errors
disp(' ');
disp(strcat('Comparison results for the set:{',num2str(r1),'<|x|<=',num2str(r2),'}'));
disp('--ANN---|hANN:Gd,nu|hANN:nu_e|hANN:G_e');
disp(strcat(num2str(err_inf_1),'|',num2str(err_inf_2),'|',num2str(err_inf_3),'|',num2str(err_inf_4)));
r1=r2;r2=r2+0.25;
end;

