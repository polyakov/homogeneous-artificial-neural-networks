function [A B C]=train_ann_dyn(N_)
% Least square traing of ANN

n=6; %number of inputs 
k=3; %number of neurons in the output layer
%N_ - a number of neurons in the hidden layer
A=randn(N_,n);   
B=randn(N_,1);

%data generation
xl=[];
while size(xl,2)<20000
 x=1.25*(rand(n,1)-0.5)*2;
 nx=norm(x);
if (nx>=0.95) && (nx<=1.05) xl=[xl x];end;
end;




sigma=@(r) 1./(1+exp(r));%sigmoid activation function

%traing rule J:=(W*C-Q)^2 -> min_C

E=eye(k);
W=[]; Q=[]; 

for i=1:size(xl,2)
W=[W sigma(A*xl(:,i)+B)];
Q=[Q rhs(xl(:,i))];
end;

C=Q/W;

