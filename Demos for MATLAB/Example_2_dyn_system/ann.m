function y=ann(x, A, B, C, a_fun)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The function y=hann(x, A, B, C)                    
%% computes the value of  ANN     
%%                                      
%%      y=C*sigma(A*x+B)
%%
%% where sigma(r)=1/(1+exp(r))
%%
%% The input parameters: 
%%     x  -  vector (n x 1)             - argument of the function 
%%     A  -  matrix (N x n)         \                              
%%     B  -  vector (N x 1)          >  - parameters of ANN
%%     C  -  matrix (1 x N)         /
%%                                                                      
%% The output parameters:                                                
%%     y - scalar (1 x 1)               - value of the function
%%
%%
%% The function y=hann(x, A, B, C, a_fun)                    
%% computes the value of homogeneous ANN 
%%
%% y=C*a_fun(A*x+B)
%%
%% where a_fun is a user defined activation function (sigmoid by default) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
if nargin<8 a_fun=@(r)1./(1+exp(r)); end;
y=C*a_fun(A*x+B);